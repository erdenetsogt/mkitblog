import React, { Component } from 'react';
import './App.css';
import Category from './components/Category';
import Home from './components/Home';
import Header from './components/Header';
import { Route, Switch } from 'react-router-dom';

class App extends Component {
	render() {
		return (
			<div className="App">
				{/* <Header/>				 */}
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/category/:id" component={Category} />													
				</Switch>				
			</div>
		);
	}
}

export default App;

