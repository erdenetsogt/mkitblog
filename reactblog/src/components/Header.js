import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {

	constructor(props) {
		super(props);
		this.state = {
			categories: [],
			open: false
		}
	}

	componentDidMount() {
		const _this = this;
		fetch('/api/category').then(function (response) {
			return response.json();
		}).then(function (data) {
			_this.setState({
				categories: data
			})
		});
	}

	toggleMenu = e => {
		e.preventDefault();
		const { open } = this.state;		
		this.setState({
			open: !open
		});
	}

	render() {
		const { open, categories } = this.state;
		return (
			<div className="navbar is-dark">
				<div className="navbar-brand">
					<Link to="/" className="navbar-item">
						Blog
					</Link>
					
					<a href="#toggleMenu" role="button" className={`navbar-burger burger ${open ? 'is-active' : ''}`} onClick={this.toggleMenu}>
						<span/><span/><span/>
					</a>
				</div>

				<div className={`navbar-menu ${open ? 'is-active' : ''}`}>
					<div className="navbar-end">
						{
							categories.map(category => (
								<Link key={category.id} to={`/category/${category.id}`} className="navbar-item">
									{category.name}
								</Link>
							))
						}

					</div>
				</div>
			</div>
		)
	}
}
