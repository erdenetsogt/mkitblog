import React, { Component } from 'react';

class Category extends Component {

	constructor(props) {
		super(props);
		this.state = {
			category: null
		}
	}

	componentDidMount() {
		const { match } = this.props;
		const categoryId = match.params.id;
		const _this = this;
		fetch(`/api/category/${categoryId}`).then(function (response) {
			return response.json();
		}).then(function (data) {
			_this.setState({
				category: data
			})
		});
	}

	componentDidUpdate(prevProps) {		
		if (this.props.match.params.id !== prevProps.match.params.id) {
			const categoryId = this.props.match.params.id;
			const _this = this;
			fetch(`/api/category/${categoryId}`).then(function (response) {
				return response.json();
			}).then(function (data) {
				_this.setState({
					category: data
				})
			});
		}
	}

	render() {		
		const { category } = this.state;
		return (
			<div>
				{
					category &&
					<h1 className="title is-1">{category.name}</h1>
				}
			</div>
		);
	}
}

export default Category;
