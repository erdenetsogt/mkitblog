import React, { Component } from 'react';

const nodeSize = 20;
const containerWidth = 10;
const containerHeight = 10;
class Home extends Component {

	constructor(props) {
		super(props);
		this.state = {
			body: [{ x: 2, y: 1 }, { x: 1, y: 1 }, { x: 0, y: 1 }],
			direction: 'right',
			food: { x: 5, y: 5 }
		}

		const {body} = this.state;
		let foodOnSnake = true;
		let newFood;
		while (foodOnSnake) {
			foodOnSnake = false;
			newFood = {
				x: Math.floor(Math.random() * containerWidth),
				y: Math.floor(Math.random() * containerHeight)
			}
			for (var i = 0; i < body.length; i++) {
				if (newFood.x === body[i].x && newFood.y === body[i].y) {
					foodOnSnake = true;
					break;
				}
			}
		}

		this.state.food = newFood;
	}

	componentDidMount() {
		document.addEventListener("keydown", this.handleKeyPress, false);

		const _this = this;

		setInterval(function () {
			const { direction, body, food } = _this.state;
			if (body[0].x === food.x && body[0].y === food.y) {
				body.push(body[body.length - 1]);

				let foodOnSnake = true;
				let newFood;
				while (foodOnSnake) {
					foodOnSnake = false;
					newFood = {
						x: Math.floor(Math.random() * containerWidth),
						y: Math.floor(Math.random() * containerHeight)
					}
					for (var i = 0; i < body.length; i++) {
						if (newFood.x === body[i].x && newFood.y === body[i].y) {
							foodOnSnake = true;
							break;
						}
					}
				}

				_this.setState({
					body: body,
					food: newFood
				})
			}

			for (var i = 1; i < body.length; i++) {
				if (body[0].x === body[i].x && body[0].y === body[i].y) {
					alert('game over');
					_this.setState({
						body: [{ x: 2, y: 1 }, { x: 1, y: 1 }, { x: 0, y: 1 }],
						direction: 'right',
						food: { x: 5, y: 5 }
					});
				}
			}

			switch (direction) {
				case 'right':
					_this.gotoRight();
					break;
				case 'left':
					_this.gotoLeft();
					break;
				case 'up':
					_this.gotoUp();
					break;
				case 'down':
					_this.gotoDown();
					break;
			}

		}, 200);
	}

	gotoRight = () => {
		const { body } = this.state;

		var position;

		if (body[0].x === containerWidth - 1) {
			position = 0;
		}
		else {
			position = body[0].x + 1;
		}

		body.unshift({ x: position, y: body[0].y })
		body.pop();

		this.setState({
			body: body
		});
	}

	gotoDown = () => {
		const { body } = this.state;

		var position;
		if (body[0].y === containerHeight - 1) {
			position = 0;
		}
		else {
			position = body[0].y + 1;
		}

		body.unshift({ x: body[0].x, y: position })
		body.pop();

		this.setState({
			body: body
		});

	}

	gotoLeft = () => {
		const { body } = this.state;

		var position;
		if (body[0].x === 0) {
			position = containerWidth - 1;
		}
		else {
			position = body[0].x - 1;
		}

		body.unshift({ x: position, y: body[0].y })
		body.pop();

		this.setState({
			body: body
		});
	}

	gotoUp = () => {
		const { body } = this.state;

		var position;
		if (body[0].y === 0) {
			position = containerHeight - 1;
		}
		else {
			position = body[0].y - 1;
		}
		body.unshift({ x: body[0].x, y: position })
		body.pop();

		this.setState({
			body: body
		});
	}

	handleKeyPress = (e) => {
		const { direction } = this.state;
		switch (direction) {
			case 'up':
			case 'down':
				switch (e.key) {
					case 'ArrowRight':
						this.setState({
							direction: 'right'
						});
						break;
					case 'ArrowLeft':
						this.setState({
							direction: 'left'
						});
						break;
				}
				break;
			case 'right':
			case 'left':
				switch (e.key) {
					case 'ArrowUp':
						this.setState({
							direction: 'up'
						});
						break;
					case 'ArrowDown':
						this.setState({
							direction: 'down'
						});
						break;
				}

		}

	}

	render() {
		const { body, food } = this.state;
		return (
			<div className="App" style={{ display: 'flex', height: '100vh', backgroundColor: this.state.bg, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
				<div style={styles.container}>
					<div style={{ ...styles.food, left: food.x * nodeSize, top: food.y * nodeSize }}>
					</div>

					{
						body.map((node, index) => (
							<div style={{ ...styles.snakeBody, background: index === 0 ? 'yellow' : 'green', left: node.x * nodeSize, top: node.y * nodeSize }}>
							</div>
						))
					}


				</div>

			</div >
		);
	}
}

const styles = {
	container: {
		width: containerWidth * nodeSize,
		height: containerHeight * nodeSize,
		background: 'black',
		position: 'relative'
	},
	snakeBody: {
		width: nodeSize,
		height: nodeSize,
		position: 'absolute'
	},
	food: {
		width: nodeSize,
		height: nodeSize,
		background: 'red',
		position: 'absolute'
	}
}

export default Home;
