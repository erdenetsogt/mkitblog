<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="blogs" ignore="true" />

<c:forEach var="blog" items="${blogs}">
	<a href="/${blog.id}" class="card mb-2 text-dark" style="text-decoration: none;">
		<div class="card-body d-flex justify-content-between">
			<div>
				<h5 class="card-title">${blog.name}</h5>
				<p class="card-text">${blog.desc}</p>
			</div>
			<c:if test="${!empty blog.image}">
				<div style="margin-left: 20px;">
					<img style="height: 150px;" src="/${blog.image.path}" />
				</div>
			</c:if>
		</div>
	</a>
</c:forEach>