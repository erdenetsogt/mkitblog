<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="categories" ignore="true" />

<ul class="navbar-nav">
	<c:forEach var="category" items="${categories}">
		<li class="nav-item"><a class="nav-link" href="/category/${category.id}">${category.name}</a></li>
	</c:forEach>
</ul>
