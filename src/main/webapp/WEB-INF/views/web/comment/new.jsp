<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form modelAttribute="form" id="commentForm" onSubmit="sendComment(); return false;">
	<form:hidden path="blogId"/>
	<div class="form-group">
		<form:label path="name">Нэр</form:label>
		<form:input path="name" class="form-control" />
	</div>
	<div class="form-group">
		<form:label path="text">Сэтгэгдэл</form:label>
		<form:textarea path="text" class="form-control" />
	</div>
	<button type="submit" class="btn btn-primary">Илгээх</button>
</form:form>

<script>
	var sendComment = function(){			
		$.post("/comments/save", $("#commentForm").serialize(), function() {
			commentNew();			
			commentList();
		});		
	}
</script>