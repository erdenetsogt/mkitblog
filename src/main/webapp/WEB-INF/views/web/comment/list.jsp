<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:forEach var="comment" items="${comments}">
	<div class="card mb-1">
		<div class="card-body">
			<strong>${comment.name}: </strong> <small class="text-muted">${comment.created}</small>
			<div>${comment.text}</div>
		</div>
	</div>
</c:forEach>