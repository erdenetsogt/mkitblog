<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container mt-5 mb-5" style="max-width: 700px;">

	<tiles:insertTemplate template="/WEB-INF/views/web/common/components/blogs.jsp" preparer="blogPreparer">
		<tiles:putAttribute name="size" value="5"/>
	</tiles:insertTemplate>
	
	<div class="d-flex justify-content-between mt-4">
		<div></div>
		<a href="/list?page=1" class="btn btn-info">Дараах</a>
	</div>
</div>

