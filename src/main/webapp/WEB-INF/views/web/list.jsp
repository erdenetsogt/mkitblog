<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container mt-5 mb-5" style="max-width: 700px;">
	
	<c:if test="${!empty category}">
		<h1 class="text-center mb-5">${category.name}</h1>
	</c:if>
	
	<c:forEach var="blog" items="${blogs.content}">
		<a href="/${blog.id}" class="card mb-2 text-dark"
			style="text-decoration: none;">
			<div class="card-body d-flex justify-content-between">
				<div>
					<h5 class="card-title">${blog.name}</h5>
					<p class="card-text">${blog.desc}</p>
				</div>
				<c:if test="${!empty blog.image}">
					<div style="margin-left: 20px;">
						<img style="height: 150px;" src="/${blog.image.path}" />
					</div>
				</c:if>
			</div>
		</a>
	</c:forEach>

	<div class="d-flex justify-content-between mt-4">
		<c:choose>
			<c:when test="${not blogs.first}">
				<a href="?page=${blogs.number-1}" class="btn btn-info">Өмнөх</a>
			</c:when>
			<c:otherwise>
				<div></div>
			</c:otherwise>
		</c:choose>

		<c:if test="${not blogs.last}">
			<a href="?page=${blogs.number+1}" class="btn btn-info">Дараах</a>
		</c:if>
	</div>
</div>

