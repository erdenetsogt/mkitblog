<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container mt-5 mb-3" style="max-width: 700px;">
	<div class="card">
		<div class="card-body">

			<h1 class="mb-4">${blog.name}</h1>

			<div class="content">${blog.text}</div>
		</div>
	</div>
</div>

<div class="container" style="max-width: 500px;">
	<div class="card mb-1">
		<div class="card-body" id="commentNew"></div>
	</div>

	<div id="commentList"></div>
</div>

<script>
	var commentNew = function() {
		$.get("/comments/new/${blog.id}", function(data) {
			$("#commentNew").html(data);
		});
	}
	commentNew();

	var commentList = function() {
		$.get("/comments/list/${blog.id}?sort=created,desc", function(data) {
			$("#commentList").html(data);
		});
	}
	commentList();
</script>


