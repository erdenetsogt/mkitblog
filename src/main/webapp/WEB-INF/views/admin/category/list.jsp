<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${empty categories}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width: 1px;">id</th>
					<th>Нэр</th>
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${categories}" var="category">
					<tr>
						<td>${category.id}</td>
						<td>${category.name}</td>
						<td style="white-space: nowrap;">

							<button class="btn btn-dark" type="button"
								onclick="editThis(${category.id})">Засах</button>
							<button class="btn btn-danger" type="button"
								onclick="deleteThis(${category.id})">Устгах</button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>


	</c:otherwise>
</c:choose>



<script>

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/category/" + id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList();			  	   
				  }
			});	
		}				
	}
</script>

