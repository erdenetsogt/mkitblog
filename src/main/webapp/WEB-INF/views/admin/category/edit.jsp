<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Ангилал нэмэх/засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editForm">
			<form:hidden path="id" />
			<div class="form-group">
				<form:label path="name">Нэр</form:label>
				<form:input path="name" class="form-control" />
			</div>
		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>
