<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Хэрэглэгч нэмэх/засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editForm">			
			<div class="form-group">
				<form:label path="username">Нэр</form:label>
				<form:input path="username" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="password">Нууц үг</form:label>
				<form:password path="password" class="form-control" />
			</div>
			<div class="form-group">				
				<form:checkbox path="enabled" label="Идэвхтэй" />
			</div>
		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>
