<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row justify-content-center">
	<div class="col-8">
				
		<div class="d-flex justify-content-between align-items-center">
			<h1>Хэрэглэгч</h1>
			<button class="btn btn-primary" onclick="create();">Шинэ</button>
		</div>

		<div id="list"></div>
		
	</div>
</div>


<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>
	var loadList = function() {
		$.get("/admin/user/list", function(data) {
			$("#list").html(data);
		});
	}
	loadList();

	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/user/new", function(data) {
			$("#edit").html(data);
		});

	}
	
	var showRole = function(username) {
		$('#editModal').modal('show');
		$.get("/admin/authority/list?username=" + username, function(data) {
			$("#edit").html(data);
		})
	}

	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/user/" + id + "/edit", function(data) {
			$("#edit").html(data);
		})
	}

	var submitForm = function() {
		$.post("/admin/user/save", $('#editForm').serialize(), function() {
			$('#editModal').modal('hide');
			$("#edit").html('');
			loadList();
		});
	}
</script>