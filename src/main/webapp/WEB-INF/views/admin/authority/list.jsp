<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Хэрэглэгчийн эрх</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<div id="addAuthority" class="mb-3"></div>
		<table class="table table-bordered">
			<tbody>
				<c:forEach items="${authorities}" var="authority">
					<tr>
						<td>${authority.authority}</td>
						<td style="white-space: nowrap; width: 1px">
							<button class="btn btn-danger" type="button"
								onclick="deleteAuthority(${authority.id})">Устгах</button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Хаах</button>
	</div>
</div>

<script>

$.get("/admin/authority/new?username=${param.username}", function(data) {
	$("#addAuthority").html(data);
});

var deleteAuthority = function(id) {
	if (confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
		$.ajax({
			url : "/admin/authority/" + id
					+ "?${_csrf.parameterName}=${_csrf.token}",
			type : 'DELETE',
			success : function() {
				alert("Амжиллтай устлаа");
				showRole("${param.username}");
			}
		});
	}
}

var saveRole = function() {
	$.post("/admin/authority/save", $('#authorityForm').serialize(), function() {			
		showRole("${param.username}");
	});
}
</script>