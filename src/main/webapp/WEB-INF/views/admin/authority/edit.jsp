<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form modelAttribute="jspform" id="authorityForm" onsubmit="saveRole(); return false;">
	<form:hidden path="username" class="form-control" />
	<div class="input-group">
		<form:input path="authority" class="form-control" />
		<div class="input-group-append">
			<button class="btn btn-info" type="submit">Нэмэх</button>
		</div>
	</div>
</form:form>
