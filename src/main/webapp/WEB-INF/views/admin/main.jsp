<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
<head>

<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
</head>
<body style="padding-top: 5rem;">
	<div class="container">
		<nav
			class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark justify-content-between">
			<a class="navbar-brand" href="/admin">Админ</a>

			<div>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item"><a class="nav-link" href="/admin/user">Хэрэглэгч</a></li>
						<li class="nav-item"><a class="nav-link" href="/admin/category">Ангилал</a></li>
						<li class="nav-item"><a class="nav-link" href="/admin/blog">Мэдээ</a></li>
						<li class="nav-item"><a class="nav-link" href="/admin/file">Файл</a></li>
						<li class="nav-item"><a class="nav-link" href="/admin/image">Зураг</a></li>
						<li class="nav-item ml-3">
							<form action="/logout" method="post">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button class="btn btn-light">Гарах</button>
							</form>
						</li>
					</ul>
				</div>
			</div>

		</nav>
		<tiles:insertAttribute name="body" />		
	</div>
</body>
</html>