<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="d-flex justify-content-between align-items-center">
	<h1>Мэдээ нэмэх/засах</h1>
	<div>
		<a href="/admin/blog" class="btn btn-secondary">Буцах</a>
		<button type="button" class="btn btn-primary" onclick="$('#editForm').submit();">Хадгалах</button>
	</div>
</div>

<form:form modelAttribute="jspform" action="/admin/blog/save" id="editForm">
	<form:hidden path="id" />
	
	<div class="form-group">
		<div id="image"></div>
		<button type="button" class="btn btn-light" onclick="loadImages(0);">Зураг
			сонгох</button>
		<form:hidden path="imageId" />
		<div id="images"></div>
	</div>
	<div class="form-group">
		<form:label path="categoryId">Ангилал</form:label>
		<form:select path="categoryId" class="form-control">
			<form:option value="0" label="..." />
			<form:options items="${categories}" itemValue="id" itemLabel="name" />
		</form:select>
	</div>
	<div class="form-group">
		<form:label path="name">Нэр</form:label>
		<form:input path="name" cssClass="form-control" cssErrorClass="form-control is-invalid" />
		<form:errors path="name" cssClass="invalid-feedback" element="div" />
	</div>
	<div class="form-group">
		<form:label path="description">Тайлбар</form:label>
		<form:input path="description" cssClass="form-control"  cssErrorClass="form-control is-invalid"/>
		<form:errors path="description" cssClass="invalid-feedback" element="div" />
	</div>
	<div class="form-group">
		<form:label path="text">Текст</form:label>
		<form:textarea path="text" class="form-control" rows="3" />
	</div>
</form:form>

<script>
	var editor = CKEDITOR.replace('text');
	var loadImages = function(page) {
		$.get("/admin/image/select?sort=created,desc&size=10&page="+page, function(data) {
			$("#images").html(data);
		});
	}
	var selectImage = function(id) {
		$("#imageId").val(id);
		$.get("/admin/image/select/"+id, function(data) {
			$("#image").html(data);
		});		
		$("#images").html("");
	}
	
	<c:if test="${!empty jspform.imageId}">
		selectImage(${jspform.imageId});
	</c:if>	
</script>

