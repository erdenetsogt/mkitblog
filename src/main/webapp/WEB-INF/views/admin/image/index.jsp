<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row justify-content-center">
	<div class="col-12">

		<div class="d-flex justify-content-between align-items-center">
			<h1>Зураг</h1>
		</div>

		<form method="POST" id="fileForm" enctype="multipart/form-data" action="/admin/image/upload">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
			<input type="file" name="file" onchange="submitForm();"/> 
		</form>
		
		<div id="list" class="mt-3" style="overflow: hidden;"></div>

	</div>
</div>

<div id="loadingModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog text-center text-white">Хуулж байна...</div>
</div>


<script>

	var loadList = function(page) {
		$.get("/admin/image/list?sort=created,desc&size=10&page="+page, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0);
	
	var submitForm = function() {		
		$('#loadingModal').modal({
			keyboard : false,
			backdrop : 'static'
		});
		$('#fileForm').submit(); 
	}
	
</script>