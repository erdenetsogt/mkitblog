<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not (images.first && images.last)}">
	<nav class="d-flex flex-row-reverse">
		<ul class="pagination">
			<c:if test="${not images.first}">
				<li class="page-item"><a class="page-link" href="#"
					onclick="loadList(${images.number-1});">Өмнөх</a></li>
			</c:if>
			<c:if test="${not images.last}">
				<li class="page-item"><a class="page-link" href="#"
					onclick="loadList(${images.number+1});">Дараах</a></li>
			</c:if>
		</ul>
	</nav>
</c:if>

<c:choose>
	<c:when test="${images.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<c:forEach items="${images.content}" var="image">
			<a href="/${image.path}" class="d-block" download
				style="float: left;"> <img src="/${image.path}"
				class="img-thumbnail rounded d-block mr-2 mb-2"
				style="max-width: auto; height: 150px;" />
			</a>
		</c:forEach>
	</c:otherwise>
</c:choose>