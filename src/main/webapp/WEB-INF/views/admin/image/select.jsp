<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="mt-2 card">
	<div class="card-body">
		<c:choose>
			<c:when test="${images.numberOfElements eq 0}">
				<h3 class="text-secondary text-center">Зураг олдсонгүй</h3>
			</c:when>
			<c:otherwise>		
				<c:if test="${not (images.first && images.last)}">
					<nav class="d-flex flex-row-reverse">
						<ul class="pagination">
							<c:if test="${not images.first}">
								<li class="page-item"><a class="page-link" href="#" onclick="loadImages(${images.number-1});">Өмнөх</a></li>
							</c:if>					
							<c:if test="${not images.last}">
								<li class="page-item"><a class="page-link" href="#" onclick="loadImages(${images.number+1});">Дараах</a></li>
							</c:if>
						</ul>
					</nav>
				</c:if>
				<div style="overflow: hidden">
					<c:forEach items="${images.content}" var="image">
						<a href="#" class="d-block" style="float:left;" onclick="selectImage(${image.id});">
							<img src="/${image.path}" class="img-thumbnail rounded d-block mr-2 mb-2" style="max-width: auto; height: 100px;"/>
						</a>						
					</c:forEach>
				</div>							
			</c:otherwise>
		</c:choose>
	</div>	
</div>
