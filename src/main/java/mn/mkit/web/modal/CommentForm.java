package mn.mkit.web.modal;

public class CommentForm {

	private Integer blogId;
	private String name;
	private String text;
	
	public CommentForm() {
	
	}
	
	public CommentForm(Integer blogId) {
		this.blogId = blogId;
	}
		
	public Integer getBlogId() {
		return blogId;
	}

	public void setBlogId(Integer blogId) {
		this.blogId = blogId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
