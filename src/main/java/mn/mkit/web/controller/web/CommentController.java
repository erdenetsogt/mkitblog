package mn.mkit.web.controller.web;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Comment;
import mn.mkit.web.modal.CommentForm;
import mn.mkit.web.repository.CommentRepository;

@Controller
public class CommentController {
	
	@Autowired
	private CommentRepository commentRepository;	
	
	@GetMapping("comments/list/{blogId}")
	public String category (Model model, @PathVariable Integer blogId, Sort sort) {
		List<Comment> comments = commentRepository.findByBlogId(blogId, sort); 	
		model.addAttribute("comments", comments);
		return "comments";		
	}
	
	@GetMapping("comments/new/{blogId}")
	public String newForm (Model model, @PathVariable Integer blogId) {		
		model.addAttribute("form", new CommentForm(blogId));
		return "newComment";	
	}
	
	@PostMapping("comments/save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(CommentForm form) {
		Comment comment = new Comment();
		comment.setBlogId(form.getBlogId());
		comment.setCreated(new Date());
		comment.setName(form.getName());
		comment.setText(form.getText());						
		commentRepository.save(comment);		
	}
	
						
}
