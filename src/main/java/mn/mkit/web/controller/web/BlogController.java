package mn.mkit.web.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import mn.mkit.web.domain.Blog;
import mn.mkit.web.domain.Category;
import mn.mkit.web.repository.BlogRepository;
import mn.mkit.web.repository.CategoryRepository;

@Controller
public class BlogController {
	
	@Autowired
	private BlogRepository blogRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("list")
	public String list (Model model, @RequestParam(defaultValue = "0") Integer page) {
		PageRequest request = PageRequest.of(page, 5, Sort.Direction.DESC, "created");
		Page<Blog> blogs = blogRepository.findAll(request);	
		model.addAttribute("blogs", blogs);
		return "blog/list";		
	}
	
	@GetMapping("category/{categoryId}")
	public String category (Model model, @PathVariable Integer categoryId, @RequestParam(defaultValue = "0") Integer page) {
		Category category = categoryRepository.findById(categoryId).get();
		model.addAttribute("category", category);
		Page<Blog> blogs = blogRepository.findByCategory(categoryId, PageRequest.of(page, 5, Sort.Direction.DESC, "created"));	
		model.addAttribute("blogs", blogs);
		return "blog/list";		
	}
	
	@GetMapping("{id}")
	public String read (Model model, @PathVariable Integer id) {
		Blog blog = blogRepository.findById(id).get();	
		model.addAttribute("blog", blog);
		return "blog/one";		
	}
	
	
						
}
