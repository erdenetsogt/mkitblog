package mn.mkit.web.controller.admin;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import mn.mkit.web.business.FileService;
import mn.mkit.web.domain.RawFile;
import mn.mkit.web.repository.RawFileRepository;

@Controller
@RequestMapping(value="/admin/file")
public class FileAdminController {
	
	@Autowired
	private FileService service;
	
	@Autowired
	private RawFileRepository repo;
	
	@GetMapping
	public String main () {		
		return "admin/file";		
	}
				
	@PostMapping("upload")
	public String uploadFile(@RequestParam MultipartFile file) throws Exception{
		RawFile rawFile = service.uploadFile(file);
		rawFile.setCreated(new Date());		
		repo.save(rawFile);
		return "redirect:/admin/file";
	}
		
	@GetMapping("list")
	public String list (Model model, Sort sort) {		
		model.addAttribute("files", repo.findAll(sort));
		return "admin/file/list";		
	}
	
}
