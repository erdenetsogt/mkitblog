package mn.mkit.web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Authority;
import mn.mkit.web.modal.AuthorityForm;
import mn.mkit.web.repository.AuthorityRepository;

@Controller
@RequestMapping(value="/admin/authority")
public class AuthorityAdminController {
	
	@Autowired
	private AuthorityRepository repo;	
		
	@GetMapping("list")
	public String list (Model model, @RequestParam(required = false) String username) {		
		model.addAttribute("authorities", repo.findByUsername(username));
		return "admin/authority/list";		
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Authority authority = repo.findById(id).get();
		repo.delete(authority);
		
	}
		
	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(AuthorityForm form) {			
		Authority authority = new Authority();
		authority.setUsername(form.getUsername());
		authority.setAuthority(form.getAuthority());			
		repo.save(authority);		
	}
	
	@GetMapping("new")
	public String newForm (Model model, @RequestParam(required = false) String username) {
		AuthorityForm form = new AuthorityForm();
		form.setUsername(username);
		model.addAttribute("jspform",form);
		return "admin/authority/edit";	
	}
					
}


