package mn.mkit.web.controller.admin;

import java.util.Date;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Blog;
import mn.mkit.web.modal.BlogForm;
import mn.mkit.web.repository.BlogRepository;
import mn.mkit.web.repository.CategoryRepository;
import mn.mkit.web.repository.ImageRepository;

@Controller
@RequestMapping(value = "/admin/blog")
public class BlogAdminController {

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ImageRepository imageRepository;

	@GetMapping
	public String main() {
		return "admin/blog";
	}

	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		model.addAttribute("blogs", blogRepository.findByName(name == null ? "" : name, pageable));
		return "admin/blog/list";
	}

	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Blog blog = blogRepository.findById(id).get();		
		blogRepository.delete(blog);
	}

	@PostMapping("save")	
	public String save(@Valid @ModelAttribute("jspform")BlogForm form, BindingResult bindingResult, Model model) {
		
		
		if (form.getName().equals("unique")) {
			bindingResult.addError(new FieldError("jspform", "name", "custom error"));
		}
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("jspform", form);
			model.addAttribute("categories", categoryRepository.findAll());
			return "admin/blog/edit";
        }
		
		Blog blog;
		if (form.getId() == null) {
			blog = new Blog();
			blog.setCreated(new Date());
		} else {
			blog = blogRepository.findById(form.getId()).get();
		}
		blog.setName(form.getName());
		blog.setDesc(form.getDescription());
		blog.setText(form.getText());
		blog.setCategory(form.getCategoryId() == null ? null : categoryRepository.findById(form.getCategoryId()).get());
		blog.setImage(form.getImageId() == null ? null : imageRepository.findById(form.getImageId()).get());
		blogRepository.save(blog);
		return "redirect:/admin/blog";
	}

	@GetMapping("new")
	public String newForm(Map<String, Object> model) {
		model.put("jspform", new BlogForm());
		model.put("categories", categoryRepository.findAll());
		return "admin/blog/edit";
	}

	@GetMapping("{id}/edit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Blog blog = blogRepository.findById(id).get();
		BlogForm blogForm = new BlogForm();
		blogForm.setId(blog.getId());
		blogForm.setName(blog.getName());
		blogForm.setDescription(blog.getDesc());
		blogForm.setText(blog.getText());
		blogForm.setCategoryId(blog.getCategory() == null ? null : blog.getCategory().getId());
		blogForm.setImageId(blog.getImage() == null ? null : blog.getImage().getId());
		model.put("jspform", blogForm);
		model.put("categories", categoryRepository.findAll());
		return "admin/blog/edit";
	}

}
