package mn.mkit.web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.User;
import mn.mkit.web.modal.UserForm;
import mn.mkit.web.repository.UserRepository;


@Secured("ROLE_ADMIN")
@Controller
@RequestMapping(value="/admin/user")
public class UserAdminController {
	
	@Autowired
	private UserRepository repo;

	
	@GetMapping
	public String main () {		
		return "admin/user";		
	}
		
	@GetMapping("list")
	public String list (Model model) {		
		model.addAttribute("users", repo.findAll());
		return "admin/user/list";		
	}
	
	@DeleteMapping("{username}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String username) {
		User user = repo.findById(username).get();
		repo.delete(user);
		
	}
		
	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(UserForm form) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		User user = new User();						
		user.setUsername(form.getUsername());
		user.setPassword(encoder.encode(form.getPassword()));
		user.setEnabled(form.getEnabled());
		repo.save(user);		
	}
	
	@GetMapping("new")
	public String newForm (Model model) {		
		model.addAttribute("jspform", new UserForm());
		return "admin/user/edit";	
	}
	
	@GetMapping("{username}/edit")
	public String edit (@PathVariable String username, Model model) {
		User user = repo.findById(username).get();
		UserForm form = new UserForm();		
		form.setUsername(user.getUsername());				
		form.setEnabled(user.getEnabled());
		model.addAttribute("jspform", form);
		return "admin/user/edit";	
	}
			
			
}
