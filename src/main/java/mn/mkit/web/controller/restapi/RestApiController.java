package mn.mkit.web.controller.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import mn.mkit.web.domain.Category;
import mn.mkit.web.repository.CategoryRepository;

@RestController
public class RestApiController {
	
	@Autowired
	private CategoryRepository repo;
		
	@GetMapping("api/category")
	public Iterable<Category> list (Sort sort) {
		return repo.findAll(sort);			
	}
	
	@GetMapping("api/category/{id}")
	public Category	 read(@PathVariable Integer id) {
		return repo.findById(id).get();	
	}
				
}
