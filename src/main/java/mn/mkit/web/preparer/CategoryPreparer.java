package mn.mkit.web.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.PreparerException;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mn.mkit.web.repository.CategoryRepository;

@Component
public class CategoryPreparer implements ViewPreparer {
	
	@Autowired
	private CategoryRepository categoryRepository;

	public void execute(Request tilesRequest, AttributeContext attributeContext) throws PreparerException {		
		attributeContext.putAttribute("categories", new Attribute(categoryRepository.findAll()));
	}
}
