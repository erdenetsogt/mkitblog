package mn.mkit.web.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.PreparerException;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import mn.mkit.web.domain.Blog;
import mn.mkit.web.repository.BlogRepository;

@Component
public class BlogPreparer implements ViewPreparer {
	
	@Autowired
	private BlogRepository blogRepository;

	public void execute(Request tilesRequest, AttributeContext attributeContext) throws PreparerException {
		Integer size = Integer.parseInt(attributeContext.getLocalAttribute("size").toString()); 
		PageRequest pageRequest = PageRequest.of(0, size, Sort.Direction.DESC, "created");
		Page<Blog> blogs = blogRepository.findAll(pageRequest);
		attributeContext.putAttribute("blogs", new Attribute(blogs.getContent()));
	}
}
