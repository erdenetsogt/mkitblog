package mn.mkit.web.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.User;

public interface UserRepository extends PagingAndSortingRepository<User, String> {
		
}
