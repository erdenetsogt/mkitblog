package mn.mkit.web.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Category;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
		
}
