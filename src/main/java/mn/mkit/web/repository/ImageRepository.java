package mn.mkit.web.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Image;

public interface ImageRepository extends PagingAndSortingRepository<Image, Integer> {
		
}
