package mn.mkit.web.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Comment;

public interface CommentRepository extends PagingAndSortingRepository<Comment, Integer> {
	
	@Query("select c from Comment c where c.blogId=?1")
	List<Comment> findByBlogId(Integer blogId, Sort sort);
	
}
