package mn.mkit.web.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.RawFile;

public interface RawFileRepository extends PagingAndSortingRepository<RawFile, Integer> {
		
}
