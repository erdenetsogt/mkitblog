create table users (
    username varchar(50) not null primary key,
    password varchar(120) not null,
    enabled boolean not null
);

create table authorities (
	id integer unsigned auto_increment primary key,	
    username varchar(50) not null,
    authority varchar(50) not null,
    foreign key (username) references users (username) on update cascade
);

create table category (
	id integer unsigned auto_increment,
	name varchar(255),	
	ordering integer unsigned,	
	primary key(id)
);

create table raw_file (
	id integer unsigned auto_increment,
	path varchar(255) not null,	
	description varchar(255),
	size integer unsigned,
	created datetime,
	primary key(id)
);

create table image (
	id integer unsigned auto_increment,
	path varchar(255) not null,	
	description varchar(255),
	width integer unsigned,
	height integer unsigned,
	size integer unsigned,
	created datetime,
	primary key(id),
	key (created)
);

create table blog (
	id integer unsigned auto_increment,
	name varchar(255),
	description varchar(255),
	created datetime,
	text text,
	category_id integer unsigned,
	image_id integer unsigned,
	primary key(id),
	foreign key (category_id) references category(id),
	foreign key (image_id) references image(id)
);

create table comment (
	id integer unsigned auto_increment,
	name varchar(255),	
	created datetime,
	text text,
	blog_id integer unsigned,	
	primary key(id),
	foreign key (blog_id) references blog(id)
);