alter table blog add column category_id integer unsigned;
alter table blog add foreign key (category_id) references category(id);

alter table blog add column image_id integer unsigned;
alter table blog add foreign key (image_id) references image(id);
