use mkitblog;

create database mkitblog;
create user 'mkitblog'@'%' identified by 'blogmkit';
grant all privileges on mkitblog.* to 'mkitblog'@'%';
flush privileges;

create database elab;
create user 'elab'@'%' identified by 'elab';
grant all privileges on elab.* to 'elab'@'%';
flush privileges;

create database library;
create user 'library'@'%' identified by 'library';
grant all privileges on library.* to 'library'@'%';
flush privileges;

create database inventory;
create user 'inventory'@'%' identified by 'inventory';
grant all privileges on inventory.* to 'inventory'@'%';
flush privileges;

create database educenter;
create user 'educenter'@'%' identified by 'educenter';
grant all privileges on educenter.* to 'educenter'@'%';
flush privileges;


revoke all privileges on elab.* from library;
flush privileges;
